const environment  = { 
    apiUrl: 'https://localhost:44321/api/',
    hubUrl: 'https://localhost:44321/gamehub'
};

export default environment;