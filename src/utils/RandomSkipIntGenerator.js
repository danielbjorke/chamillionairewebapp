
export const getNewSkipInt = (previousInts, numberOfQuestions) => {
        let randInt = getRandomInt(numberOfQuestions);
    if (previousInts.includes(randInt) && (previousInts.length < numberOfQuestions)) {
        return getNewSkipInt(previousInts);
    } else {
        return randInt;
    }
};

const getRandomInt = upperBound => {
    return Math.floor(Math.random()*upperBound)
}