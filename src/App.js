import './App.css';
import Navbar from "../src/components/containers/navbar";
import Game from "../src/components/containers/Game/Game";
import Dashboard from "../src/components/containers/dashboard";
import JoinGame from "../src/components/containers/JoinGame/JoinGame";
import CreateGame from "../src/components/containers/CreateGame/CreateGame";
import Questions from "../src/components/containers/Questions/Questions";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";



function App() {

  
  return (
    
    <Router basename={process.env.PUBLIC_URL}>
      <div className="App">
        <Navbar />

        <Switch>
          <Route path="/dashboard" component={Dashboard} />
          <Route path="/game" component={Game} />
          <Route path="/join" component={JoinGame} />
          <Route path="/create" component={CreateGame} />
          <Route path="/question" component={Questions} />
          <Route path="/questions" component={Questions} />

        </Switch>
      </div>

    </Router>
  );
}

export default App;