import { 
    SET_CURRENT_QUESTION,
    SET_CHAMELEON_BOOL,
    SET_SECRET_ANSWER,
    ADD_VOTE,
    RESET_VOTES
} from './actionTypes';


export const setCurrentQuestion = question => ({
    type: SET_CURRENT_QUESTION,
    question
})

export const setChameleonBool = chameleonBool => ({
    type: SET_CHAMELEON_BOOL,
    chameleonBool
})

export const setSecretAnswer = answer =>({
    type: SET_SECRET_ANSWER,
    answer
})

export const addVote = vote => ({
    type: ADD_VOTE,
    vote
})

export const resetVotes = () =>({
    type: RESET_VOTES
})