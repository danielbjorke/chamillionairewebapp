import { 
    NEW_GAME,
    ADD_SKIP_INT, 
    SET_NUMBER_OF_QUESTIONS, 
} from "./actionTypes";

export const newGameAction = () => ({
    type: NEW_GAME,
});


export const addSkipIntAction = skipInt => ({
    type: ADD_SKIP_INT,
    skipInt
});

export const setNumberOfQuestions = numberOfQuestions => ({
    type: SET_NUMBER_OF_QUESTIONS,
    numberOfQuestions
});

