import { HubConnectionBuilder, withCallbacks, signalMiddleware } from 'redux-signalr';
import environment from '../../environment/environment';
import {addPlayerAction, setPlayersAction} from '../actions/playersAction';
import {getNewSkipInt} from '../../utils/RandomSkipIntGenerator';
import { addSkipIntAction } from '../actions/gameAction';
import {setCurrentQuestion, setChameleonBool, setSecretAnswer, addVote} from '../actions/questionAction';


export const connection = new HubConnectionBuilder()
.withUrl(`${environment.hubUrl}`)
.withAutomaticReconnect()
.build();


const callbacks = withCallbacks()
    .add('JoinGame', (player) => (dispatch) => {
        console.log('new player joined', player);
        dispatch(addPlayerAction(player))
        dispatch(updateLobby());
    })
    .add('CurrentLobby', (players) => (dispatch) => {
        console.log('New list of players', players);
        dispatch(setPlayersAction(players));
    })
    .add('AssignChameleon', () => (dispatch) =>{
        dispatch(setChameleonBool(true))
        console.log("You are the chameleon")
    })
    .add('SendQuestion', (question) => (dispatch) => {
        console.log("Question")
        console.log(question)
        dispatch(setCurrentQuestion(question))
    }) 
    .add('SecretAnswer', (answer) => (dispatch) => {
        dispatch(setSecretAnswer(answer))
        dispatch(setChameleonBool(false))
        console.log("Secret answer: ", answer)
    })
    .add('NewVote', (voter, chameleonCandidate) => (dispatch) => {
        dispatch(addVote({voter, chameleonCandidate}));
    })
    

 
export const newRound = (sessionPlayer) => (dispatch, getState, invoke) =>
{
    console.log("SessionPlayer: ", sessionPlayer )
    if(sessionPlayer.isHost){
        console.log("newRound");
        let players = getState().game.players;
        let previousSkipInts = getState().game.previousSkipInts;
        console.log("players: ",players);
        let numberOfQuestions = getState().game.numberOfQuestions;
        console.log("numberOfQuestions: ", numberOfQuestions)
        let randSkipInt = getNewSkipInt(previousSkipInts, numberOfQuestions);
        console.log("randSkipInt: ", randSkipInt)
        dispatch(addSkipIntAction(randSkipInt));
        invoke('NewRound', sessionPlayer.gamePin, randSkipInt, players);
    }
};

export const joinRoom = (player) => (dispatch, getState, invoke) => {
    invoke('JoinRoom', player)
};

export const updateLobby = () => (dispatch, getState, invoke) => {
    if(getState().session.isHost){
        console.log("----updateLobby----");
        let players = getState().game.players;
        console.log("invoking updateLobby with players: ", players)
        invoke('UpdateLobby', players)
    }
};

export const vote = (chameleonCandidate) => (dispatch, getState, invoke) => {
    invoke('Vote', getState.session, chameleonCandidate)
}



export const signal = signalMiddleware({
    callbacks,
    connection,
})