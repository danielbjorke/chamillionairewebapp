const { 
    SET_PLAYERS, 
    ADD_PLAYER, 
    NEW_GAME, 
    ADD_SKIP_INT, 
    SET_NUMBER_OF_QUESTIONS,
} = require("../actions/actionTypes")

const initialState ={ 
    players: [], 
    previousSkipInts: [],
    numberOfQuestions: 0,
};

const gameReducer = (state = initialState, action = {}) => {
    switch (action.type) {

        case SET_PLAYERS:
            return {...state, 
                players: action.players};
        case ADD_PLAYER:
            return {...state,
                players: [...state.players, action.player]};

        case NEW_GAME:
            return initialState;

        case ADD_SKIP_INT:
            return {...state,
                previousSkipInts: [...state.previousSkipInts, action.skipInt]};

        case SET_NUMBER_OF_QUESTIONS:
            return {...state,
            numberOfQuestions: action.numberOfQuestions
        }
        
        default:
            return state;
    }
}

export default gameReducer;