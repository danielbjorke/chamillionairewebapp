const {
    SET_CURRENT_QUESTION,
    SET_CHAMELEON_BOOL,
    SET_SECRET_ANSWER,
    ADD_VOTE,
    RESET_VOTES
} = require('../actions/actionTypes');


const initialState = {
    question: {
        Category: "",
        Alternatives: ""
    },
    secretAnswer: "",
    chameleonBool: false,
    votes: []
};

const currentQuestionReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case SET_CURRENT_QUESTION:
            return {
                ...state,
                question: action.question
            }

        case SET_CHAMELEON_BOOL:
            return {
                ...state,
                chameleonBool: action.chameleonBool
            }

        case SET_SECRET_ANSWER:
            return {
                ...state,
                secretAnswer: action.answer
            }
        case ADD_VOTE:
            return {
                ...state,
                votes: [state.votes, action.vote]
            }
        
        case RESET_VOTES:
            return {
                ...state,
                votes: []
            }

        default:
            return state;
    }
}

export default currentQuestionReducer;