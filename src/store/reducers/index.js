import { combineReducers } from "redux";
import sessionReducer from "./sessionReducer";
import gameReducer from "./gameReducer";
import currentQuestionReducer from "./currentQuestionReducer";

export default combineReducers({
    session: sessionReducer,
    game: gameReducer,
    currentQuestion: currentQuestionReducer
});
