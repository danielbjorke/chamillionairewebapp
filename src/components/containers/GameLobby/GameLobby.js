import GameWindow from './GameWindow/GameWindow';
import { useDispatch, useSelector } from 'react-redux';
import {newRound} from '../../../store/helpers/createSignalRMiddleware';
import { getQuestionNumbers } from '../../../api/GameService';
import { setNumberOfQuestions } from '../../../store/actions/gameAction';

const GameLobby = () => {
    const dispatch = useDispatch();
    
    const sessionPlayer = useSelector(state => state.session)


    const start = async() => {
        getQuestionNumbers()
        .then(result =>  {
            dispatch(setNumberOfQuestions(result.data));
            dispatch(newRound(sessionPlayer));
        });
    }

    return (
        <div>
            <h3>Game lobby!</h3>
                <GameWindow />
                <button onClick={start}>
                    Start Game
                </button>
        </div>
    )
};

export default GameLobby;