import React from 'react';

const EventLog = (props) => (
    <div style={{ background: "#eee", borderRadius: '5px', padding: '0 10px' }}>
        <p>{props.user}</p>
        <p>Is host: {props.isHost.toString()}</p>
    <p>Game pin: {props.gamePin}</p>
    </div>
);

export default EventLog;