import React from 'react';
import { useSelector } from 'react-redux';
import {CopyToClipboard} from 'react-copy-to-clipboard';


const GameWindow = () => {
    const gamePin = useSelector(state => state.session.gamePin);
    console.log(gamePin)
    return (
        <div>
            Game Lobby for game:
            <br/>
            {gamePin}
            <br/>
            <CopyToClipboard text={gamePin}>
                <button>Copy to clipboard</button>
                </CopyToClipboard>
        </div>
    )};

export default GameWindow;